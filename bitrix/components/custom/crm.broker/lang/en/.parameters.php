<?
$MESS["CRM_CONTACT_VAR"] = "Broker ID Variable Name";
$MESS["CRM_SEF_PATH_TO_INDEX"] = "Index Page Path Template";
$MESS["CRM_SEF_PATH_TO_LIST"] = "Broker Page Path Template";
$MESS["CRM_SEF_PATH_TO_EDIT"] = "Broker Editor Page Path Template";
$MESS["CRM_SEF_PATH_TO_SHOW"] = "Broker View Page Path Template";
$MESS["CRM_SEF_PATH_TO_SERVICE"] = "Web Service Page Path Template";
$MESS["CRM_SEF_PATH_TO_EXPORT"] = "Export Page Path Template";
$MESS["CRM_SEF_PATH_TO_IMPORT"] = "Import Page Path Template";
$MESS["CRM_ELEMENT_ID"] = "Broker ID";
$MESS["CRM_NAME_TEMPLATE"] = "Name format";
?>