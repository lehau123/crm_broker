<?php
AddEventHandler('crm', 'OnAfterCrmControlPanelBuild', ['BuildMenuCrm','OnAfterCrmControlPanelBuild']);

class BuildMenuCrm{
	static public function OnAfterCrmControlPanelBuild( &$items){
		//addMessage2Log($items,__LINE__.__FILE__);
		$items[] = array(
			'ID'   => 'BROKER',
			'MENU_ID' => 'menu_crm_broker',
			'TEXT' => 'Broker',
			'NAME' => 'Broker',
			'TITLE'=> 'Broker',
			'URL'  => '/crm/broker/',
			'ICON' => 'broker',
			'ACTIONS' => array(
				array(
					'ID' => 'CREATE',
					'URL' =>  '/crm/broker/details/0'
				)
			),
		);
		//addMessage2Log($items,__LINE__.__FILE__);
		return $items;
	}
}
?>