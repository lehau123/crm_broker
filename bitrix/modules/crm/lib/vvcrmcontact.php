<?php 
namespace Bitrix\Crm;
\CModule::IncludeModule('crm');

class VVCrmContact extends \CCrmContact {
	static public $sUFEntityID = 'CRM_CONTACT';
	const USER_FIELD_ENTITY_ID = 'CRM_CONTACT';
	const SUSPENDED_USER_FIELD_ENTITY_ID = 'CRM_CONTACT_SPD';

	public $LAST_ERROR = '';
	public $cPerms = null;
	protected $bCheckPermission = true;
	const TABLE_ALIAS = 'L';
	protected static $TYPE_NAME = 'CONTACT';
	private static $FIELD_INFOS = null;
	const DEFAULT_FORM_ID = 'CRM_CONTACT_SHOW_V12';


	// GetList with navigation support
	public static function GetListEx($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array(), $arOptions = array())
	{
		if(!is_array($arOptions))
		{
			$arOptions = array();
		}

		if(!isset($arOptions['PERMISSION_SQL_TYPE']))
		{
			$arOptions['PERMISSION_SQL_TYPE'] = 'FROM';
			$arOptions['PERMISSION_SQL_UNION'] = 'DISTINCT';
		}

		$lb = new \CCrmEntityListBuilder(
			\CCrmContact::DB_TYPE,
			\CCrmContact::TABLE_NAME,
			self::TABLE_ALIAS,
			self::GetFields(isset($arOptions['FIELD_OPTIONS']) ? $arOptions['FIELD_OPTIONS'] : null),
			self::$sUFEntityID,
			'CONTACT',
			[],//array('CCrmContact', 'BuildPermSql'),
			array('CCrmContact', '__AfterPrepareSql')
		);

		return $lb->Prepare($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $arOptions);
	}
}




?>